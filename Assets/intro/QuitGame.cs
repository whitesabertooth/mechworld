﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {
	
	void OnMouseDown() {
		Debug.Log ("Quit Game");
		Application.Quit ();
	}
}
