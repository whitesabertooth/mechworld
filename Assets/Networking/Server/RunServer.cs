﻿using UnityEngine;
using System.Collections;
using Cubiquity;

[RequireComponent(typeof(PhotonView))]
public class RunServer : Photon.MonoBehaviour {

	private ArrayList allLoadedChunks;
	private bool roomSetup;
	private voxelfile voxel;
	public string voxelfileid;
	public string voxelfiledata;
	private bool voxelopen;
	//action enum
	private enum PLAYER_ACTION
	{
		NOOP = 0,
		FORWARD = 1,
		BACKWARD = 2,
		RIGHT = 3,
		LEFT = 4,
		ACTION_ONE = 5,
		ACTION_TWO = 6,
		ACTION_THREE = 7,
		ACTION_FOUR = 8
	};

	private struct chunkData
	{
		public chunkData(int _x,int _y, int _z, GameObject _obj)
		{
			x = _x;
			y = _y;
			z = _z;
			voxel = _obj;
		}
		public int x;
		public int y;
		public int z;
		public GameObject voxel;

	}
	private int currentChunkSize;
	void loadChunk(int _x, int _y, int _z)
	{
		if (!voxelopen)
		{
			voxel.open (voxelfileid, voxelfiledata);
			voxel.load();
			voxelopen = true;
			currentChunkSize = voxel.getChunkSize();
		}
		ColoredCubesVolumeData data;
		data = voxel.getChunk (_x, _y, _z);
		if (null == data) {
			//Create new chunk
			data = voxel.createNewChunk();
		}
		GameObject obj = ColoredCubesVolume.CreateGameObject (data, true, true);
		//Transform the Chunk
		obj.transform.position = new Vector3 (currentChunkSize * _x, currentChunkSize * _y, currentChunkSize * _z);
		//And add PhotonView to it
		int id1 = PhotonNetwork.AllocateViewID();
		//Set the PhotonView
		PhotonView nView = obj.AddComponent<PhotonView>();
		nView.viewID = id1;
		//Attach a script to compress the chunk and uncompress on send/receive serialize

		//Save chunk
		chunkData achunk = new chunkData (_x, _y, _y, obj);
		allLoadedChunks.Add (achunk);

	}

	void saveAllChunks()
	{
		//Cycle through all chunks and save them
		chunkData achunk;
		ColoredCubesVolume data;
		for(int i = 0; i<allLoadedChunks.Count; i++)
		{
			achunk = (chunkData)allLoadedChunks[i];
			data = achunk.voxel.GetComponent<ColoredCubesVolume>();
			if(data != null && data.data != null)//just a safeguard
			{
				voxel.saveChunk(achunk.x,achunk.y,achunk.z,data.data);
			}
		}
	}

	void Awake()
	{
		if (!photonView.isMine) {
				//We aren't the photonView owner, disable this script
				//RPC's and OnPhotonSerializeView will STILL get through but we prevent Update from running
				enabled = false;
		}
		roomSetup = false;
		voxel = new voxelfile ();
		voxelopen = false;
		allLoadedChunks = new ArrayList ();
		currentChunkSize = 0;
	}

	// Use this for initialization
	void Start () {
		SetupNetwork ();
	}

	void SetupNetwork()
	{
		//Setup Network, join room
		PhotonNetwork.ConnectUsingSettings("1.0");
		//if (photonView.isMine)//  Check if I own the PhotonView
		//Initialize World
	}

	//public int width,height,depth;//World size

	void InitializeWorld()
	{
		//Load world from file
		// Create an empty ColoredCubesVolumeData with dimensions width * height * depth
		//ColoredCubesVolumeData data = VolumeData.CreateEmptyVolumeData<ColoredCubesVolumeData>(new Region(0, 0, 0, width-1, height-1, depth-1));
		//Get gameobject: coloredCubesVolume = gameObject.GetComponent<ColoredCubesVolume>();
		//Erase voxel
		//coloredCubesVolume.data.SetVoxel(voxel.x, voxel.y, voxel.z, new QuantizedColor(0,0,0,0));
		// Get the current color of the voxel
		//QuantizedColor color = coloredCubesVolume.data.GetVoxel(x, y, z);	
		loadChunk (0, 0, 0);

		// Perform the raycasting. If there's a hit the position will be stored in these ints.
		//PickVoxelResult pickResult;
		//bool hit = Picking.PickFirstSolidVoxel(coloredCubesVolume, ray, 1000.0f, out pickResult);
				// If we hit a solid voxel then create an explosion at this point.
		//if(hit)
		//{					
		//	int range = 5;
		//	DestroyVoxels(pickResult.volumeSpacePos.x, pickResult.volumeSpacePos.y, pickResult.volumeSpacePos.z, range);
		//}
	}
	/*==========================================
	//For voxels, determine if surface voxel
	public bool IsSurfaceVoxel(int x, int y, int z)
	{
		QuantizedColor quantizedColor;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z);
		if(quantizedColor.alpha < 127) return false;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x + 1, y, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x - 1, y, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y + 1, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y - 1, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z + 1);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z - 1);
		if(quantizedColor.alpha < 127) return true;
		
		return false;
	}
	==========================================*/

	void OnJoinedLobby()
	{
		Debug.Log("We joined the lobby.");
		if(!roomSetup)//only setup room one initial lobby load
		{
			SetupPUNRoom();
			InitializeWorld();
		}
	}

	void OnConnectedToPhoton()
	{
		Debug.Log("This client has connected to a server");

	}
	
	void OnDisconnectedFromPhoton()
	{
		Debug.Log("This client has disconnected from the server");
	}
	
	void OnFailedToConnectToPhoton(ExitGames.Client.Photon.StatusCode status)
	{
		Debug.Log("Failed to connect to Photon: " + status);
	}

	void SetupPUNRoom()
	{
		RoomOptions roomOptions = new RoomOptions () { isVisible = false, maxPlayers = 4 };
		PhotonNetwork.JoinOrCreateRoom ("Room1", roomOptions, TypedLobby.Default);
		Debug.Log ("Created room1");
		roomSetup = true;
	}

	
	//PLAYER EVENTS
	void OnPhotonPlayerConnected(PhotonPlayer player)
	{
		Debug.Log("Player connected: " + player);
		//See if previous player data exists, if so send it by RPC
		//If not create new player data and send it
	}
	
	void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		Debug.Log("Player disconnected: " + player);
		//save player data and send remove player by RPC
		
	}

	//Local call to setup player when enters room
	void SetupNewPlayer() {
	}

	//Make all network code reusable in single-player by setting:
	//PhotonNetwork.offlineMode = true;

	//==================================================
	//Automatic spawning
	//void SpawnMyPlayerEverywhere()
	//{
	//	PhotonNetwork.Instantiate("MyPrefabName", new Vector3(0,0,0), Quaternion.
	//	                          identity, 0);
	//	//The last argument is an optional group number, feel free to ignore it for now.
	//}
	//==================================================

	//==================================================
	//Manual Spawning...
	void SpawnMyPlayerEverywhere()
	{
		//Manually allocate PhotonViewID
		int id1 = PhotonNetwork.AllocateViewID();
		photonView.RPC("SpawnOnNetwork", PhotonTargets.AllBuffered, transform.position,
		               transform.rotation, id1, PhotonNetwork.player);
	}
	public Transform playerPrefab; //set this in the inspector
	[RPC]
	void SpawnOnNetwork(Vector3 pos, Quaternion rot, int id1, PhotonPlayer np)
	{
//		Transform newPlayer = Instantiate(playerPrefab, pos, rot) as Transform;
		//Set the PhotonView
//??		PhotonView[] nViews = go.GetComponentsInChildren<PhotonView>();
//		nViews[0].viewID = id1;
	}
	//==================================================



	//Local call to delete player when leaves room
	void DeletePlayer() {
	}

	//For loading levels
	// Temporary disable processing of futher network messages
	//PhotonNetwork.isMessageQueueRunning = false;
	//Application.LoadLevel(levelName);
	//Alternatively you can use PhotonNetwork.LoadLevel. It temporarily disables the message queue as well.

	//Remote call for button press by player and how long pressed
	[RPC]
	void PlayerAction(int action, float duration, PhotonMessageInfo info)
	{
		//sent from info.sender.ID;
		//time of vlidity info.timestamp;
		//PhotonNetwork.time is the current common network time
		PLAYER_ACTION act = (PLAYER_ACTION)action;
		//action is an enumeration
		switch (act) {
			case PLAYER_ACTION.NOOP:
				break;
		}

		//send ack/nack
		//PhotonView photonView = PhotonView.Get(this);
		//photonView.RPC("ChatMessage", PhotonTargets., "jup", "and jup!");
	}

	// Update is called once per frame
	void Update () {
	
	}

	
	//This is used only when observing this script via a PhotonView.
	//Send player data and npc data to all users
	//World data will be taken care of in another script
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		//Custom code here (your code!)
	}
}
