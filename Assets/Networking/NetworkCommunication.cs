using UnityEngine;
using System.Collections;
using Cubiquity;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public class NetworkCommunication : Photon.MonoBehaviour {
	//=================PUBLIC
	public GameObject playerPrefab; //set this in the inspector
	public GameObject playerPrefabDummy; //set this in the inspector, doesn't have controls
	public GameObject playerPrefabServer; //set this in the inspector, doesn't have controls, but does have motor
	
	public string voxelfileid;
	public string voxelfiledata;
	public string playerfiledata;
	public string localPlayerName;
	public double waittime = 1.0;
	public float PLAYER_NEAR_CHUNK = 50.0f;

	//=================PRIVATE
	private bool roomSetup;//Network game running
	private bool isServer;//Am I the server
	private bool isDedicatedServer;//Am I the dedicated server

	private GameObject spawnpoint;
	private ArrayList allLoadedChunks;
	private voxelfile voxel;
	private bool voxelopen;
	private byte localPlayerIndex;
	private Dictionary<byte,object> allPlayers;//player idx to player object
	private byte allPlayersMax;//Next Idx for Player
	private Dictionary<string,byte> playerIndex;//reference a player by name to idx
	private Dictionary<int,object> allEntities;//Entity idx to entity object
	private int allEntitiesMax;//Next Idx for Entities
	private Dictionary<int,object> allActiveEntities;//ActiveEntity idx to ActiveEntity object
	private int allActiveEntitiesMax;//Next Idx for ActiveEntities
	private bool readyToReceive;//Ready to receive Server data
	
	private struct chunkData
	{
		public chunkData(int _x,int _y, int _z, GameObject _obj)
		{
			x = _x;
			y = _y;
			z = _z;
			voxel = _obj;
		}
		public int x;
		public int y;
		public int z;
		public GameObject voxel;		
	}
	private struct sendChunkData
	{
		public sendChunkData(PhotonPlayer _pp, long _x, long _y, long _z)
		{
			player = _pp;
			x = _x;
			y = _y;
			z = _z;
		}
		public long x;
		public long y;
		public long z;
		public PhotonPlayer player;
	}
	private int currentChunkSize;

	private double lastupdatetime;
	private Camera mycamera;

	private ArrayList chunksToSend;
	private int CONST_QUANTIZE_COLOR = 4;//bytes
	private PlayerStatus playerStatusScript;
	//private ArrayList allPlayers;
	private bool firstUpdate;
	private bool singleplayermode;
	private Vector3 spawnlocation;
	private Quaternion spawnrotation;
	//=================END PRIVATE

	//=================START MonoBehaviors
	void Awake()
	{
		//Check if single player or networked
		if(!PhotonNetwork.connected)
		{
			isServer = true;
			localPlayerName = "Server";//for single player
			isDedicatedServer = false;
			roomSetup = true;
			PhotonNetwork.offlineMode = true;//Make single-player mode
			singleplayermode = true;

		} else {
			GUIScript.gameStarted = true;//Set game started
			isServer = GUIScript.getIsServer ();//Am I the server
			isServer = PhotonNetwork.isMasterClient;//Server usage
			isDedicatedServer = GUIScript.getIsDedicated ();//Dedicated Server
			roomSetup = GUIScript.getRoomSetup ();//Network game running
			singleplayermode = false;
			
			//Set default username
			if(isServer)
			{
				localPlayerName = "Server";
			} else {
				localPlayerName = "Client";
			}

			//Get username if not default
			if(GUIScript.inputUsername != GUIScript.defaultUsername)
			{
				localPlayerName = GUIScript.inputUsername;
			}
		}

		if (isServer) {
			voxel = new voxelfile ();
			voxelopen = false;
			allLoadedChunks = new ArrayList ();
			currentChunkSize = 0;
		} else {
			//ExitGames.Client.Photon.Hashtable playerproperties = new ExitGames.Client.Photon.Hashtable();
			//playerproperties.Add((object)
		}
		mycamera = FindObjectOfType<Camera> ();//Used for hooking up camera to player
		spawnpoint = GameObject.FindWithTag ("SpawnPoint");//Used for hooking up camera to player
		chunksToSend = new ArrayList ();
		firstUpdate = true;
		allPlayersMax = 0;
		allEntitiesMax = 0;
		allActiveEntitiesMax = 0;
		readyToReceive = false;

	}

	void Start()
	{
		//if (!photonView.isMine) {
		//	//We aren't the photonView owner, disable this script
		//	//RPC's and OnPhotonSerializeView will STILL get through but we prevent Update from running
		//	enabled = false;//Also doesn't call the Start method
		//}
		PhotonNetwork.isMessageQueueRunning = true;
		PhotonNetwork.playerName = localPlayerName;//Correctly sets the playerName for the network, and syncs it
		if (isServer) {
			InitializeWorld ();
			loadPlayerData();
			spawnlocation = spawnpoint.transform.position;
			spawnrotation = spawnpoint.transform.rotation;
			//TODO: update height to right above voxel

			if(!isDedicatedServer)//If just a masterclient
			{
				//Send Server Player Data
				CPlayer player = findPlayer (localPlayerName);
				if(player == null)
				{
					player = new CPlayer();
					player.updatePlayerName(localPlayerName);
					addPlayer (player,localPlayerName);
				} else {
					player.disabled = false;//re-enable server
				}

				//Need this set for Spawn.  Will be reset later
				localPlayerIndex = (byte)player.getIndex();

				SpawnMyPlayerEverywhere(localPlayerName,(byte)player.getIndex());	


				//Should be set in SpawnMyPlayerEverywhere
				//GameObject obj = GameObject.FindGameObjectWithTag("Player");
				//playerStatusScript = obj.GetComponentInChildren<PlayerStatus>();
				setLocalPlayerIndex((byte)player.getIndex());//set server info
			}

		} else {
			//No data, so make empty set
			allPlayers = new Dictionary<byte, object>();
			playerIndex = new Dictionary<string, byte> ();
			allPlayersMax = 0;

			//Client, so send login RPC to server
			photonView.RPC("PlayerInitalConnected", PhotonTargets.MasterClient, localPlayerName);
		}
		lastupdatetime = 0.0;

	}

	//This function should only be called by the Server
	void Update()
	{
		if(firstUpdate)
		{
			//On first update do a few things
			firstUpdate = false;
		}
		
		if(isServer)
		{
			//DO nothing
		} else {
			//Client Update
			//loop through all entities and see what changed
			CPlayer player = null;
			foreach(KeyValuePair<byte,object> item in allPlayers)
			{
				player = (CPlayer)item.Value;
				
				if(!player.changingEmpty())
				{
					player.clientInterpolateEntity();
				}
			}
		}
	}

	void FixedUpdate()
	{
		if(isServer)
		{
			//Server Update
			double currenttime = Time.fixedTime;
			if(lastupdatetime + waittime > currenttime) //Wait for at least a while
			{
				//Send another RPC chunk
				SendNetworkChunk();
				lastupdatetime = currenttime;
			}
		} else {
			//Client Update
		}
	}

	//Use this function to send/receive from Server to client only all the updates for entities
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if(stream.isWriting && PhotonNetwork.isMasterClient)
		{
			int entitychanges = 0;
			//First count how many entities are enabled
			foreach(KeyValuePair<byte,object> item in allPlayers)
			{
				CPlayer player = (CPlayer)item.Value;
				if(!player.disabled)
				{
					entitychanges++;
				}
			}
			entitychanges = allPlayers.Count;//Send all players
			//Send number of Player changes
			stream.Serialize(ref entitychanges);
			//Send the player changes
			short key;
			foreach(KeyValuePair<byte,object> item in allPlayers)
			{
				key = (short)item.Key;
				CPlayer player = (CPlayer)item.Value;

				if(!player.disabled)//Send everything
				{
					stream.Serialize(ref key);
					CEntity.CEntityChanging chg = player.getChanged();
					chg.PhotonSerialize(stream);
				}
			}

		} else if(!PhotonNetwork.isMasterClient) {//Server does not receive his own stuff
			if(stream.Count > 0 && readyToReceive)//if data and readyToReceive
			{
				int entitychanges = 0;
				//Receive number of Player changes
				stream.Serialize(ref entitychanges);
				//Receive the player changes
				short key = 0;
				CPlayer player = null;
				while(entitychanges>0)
				{
					entitychanges--;
					stream.Serialize(ref key);
					//TODO: Handle reconciliation better for local player data received from server
					//Currently just allow Client to predict and assume correct
					if((byte)key != localPlayerIndex)
					{
						player = (CPlayer)allPlayers[(byte)key];
						CEntity.CEntityChanging chg = new CEntity.CEntityChanging();
						chg.PhotonSerialize(stream);
						player.setChanging(chg);
					}
				}

			}
		}
	}
	//=================END MonoBehaviors
	public void getLocalPlayer(ref CPlayer player)
	{
		player = (CPlayer)allPlayers [localPlayerIndex];
	}

	//Only run on Server
	byte addPlayer(CPlayer player,string playername)
	{
		if (allPlayersMax >= byte.MaxValue)//Don't allow maxvalue either
		{
			return byte.MaxValue;
		}
		playerIndex.Add (playername, allPlayersMax);
		player.setIndex (allPlayersMax);
		allPlayers.Add (allPlayersMax++, player);
		return (byte)(allPlayersMax-1);
	}
	//Only run on Server
	int addEntity(CEntity entity)
	{
		if (allEntitiesMax > int.MaxValue)
		{
			return -1;
		}
		entity.setIndex (allEntitiesMax);
		allEntities.Add (allEntitiesMax++, entity);
		return allEntitiesMax-1;
	}
	//Only run on Server
	int addActiveEntity(CActiveEntity aentity)
	{
		if (allActiveEntitiesMax > int.MaxValue)
		{
			return -1;
		}
		aentity.setIndex (allActiveEntitiesMax);
		allActiveEntities.Add (allActiveEntitiesMax++, aentity);
		return allActiveEntitiesMax-1;
	}

	//Search for player, return null if not found
	CPlayer findPlayer(string playername)
	{
		if(playerIndex.ContainsKey(playername))
		{
			CPlayer player = (CPlayer)allPlayers[playerIndex[playername]];
			return player;
		}
		return null;
	}

	//Search for player, return byte.MaxValue if not found
	byte findPlayerIndex(string playername)
	{
		if(playerIndex.ContainsKey(playername))
		{
			return playerIndex[playername];
		}
		return byte.MaxValue;
	}

	//Client to Server RPC
	[RPC]
	void PlayerInitalConnected(string playername, PhotonMessageInfo info)
	{
		Debug.Log ("Server received new player: " + info.sender.name);
		byte idx = byte.MaxValue;
		idx = findPlayerIndex (playername);
		CPlayer player;
		if(idx == byte.MaxValue)
		{
			//Player not already setup, so create generic player
			player = new CPlayer();
			player.updatePlayerName(playername);
			idx = addPlayer(player,playername);
		} else {
			//Re enabling player (or initial enabling)
			player = (CPlayer)allPlayers[idx];
			player.disabled = false;
		}
		player.setPhotonPlayer (info.sender);
		//Send to all clients to setup allPlayers there too
		photonView.RPC ("updateEntityCommon", PhotonTargets.Others, (byte)CEntity.EEntityType.PLAYER, (int)player.getIndex(),player.getByteCommon());//Add player data to everyone else' data
		photonView.RPC ("updateEntityTransformers", PhotonTargets.Others, (byte)CEntity.EEntityType.PLAYER, (int)player.getIndex(),player.getByteTransformers());//Add player data to everyone else' data

		//Now spawn local player and send world
		SetupNewPlayer(playername,idx);//setup player on Server and send to Player
		sendAllChunkNearPlayer (player);//schedule all chunks to be sent

		//TODO: Update Changes to be current with Server State
		//photonView.RPC ("updatePlayerChanges", PhotonTargets.Others, player.getIndex(),player.change.previousServerPosition,player.change.previousServerVelocity,player.change.previousServerRotation,player.change.validNetworkTime);//Add player data to everyone else' data

		//Send all entities
		sendAllEntities (info.sender,idx);
		//Send just to Player
		photonView.RPC ("setLocalPlayerIndex", info.sender, idx);//Must be last to turn-on client receiving server updates
	}

	//Only run by Server
	void sendAllEntities(PhotonPlayer np,byte idx)
	{
		//Currently only players
		//loop through all entities and see what changed
		CPlayer player = null;
		foreach(KeyValuePair<byte,object> item in allPlayers)
		{
			player = (CPlayer)item.Value;
			if(item.Key != idx)//do not resend their local player
			{
				photonView.RPC ("updateEntityCommon", np, (byte)CEntity.EEntityType.PLAYER, (int)player.getIndex(),player.getByteCommon());//Add player data to everyone else' data
				photonView.RPC ("updateEntityTransformers", np, (byte)CEntity.EEntityType.PLAYER, (int)player.getIndex(),player.getByteTransformers());//Add player data to everyone else' data
				
				photonView.RPC("SpawnOnNetwork", np, spawnpoint.transform.position,
				               spawnpoint.transform.rotation, player.getPhotonViewID(), player.getPlayerName(), item.Key);
			}
		}
	}

	//Run on the Client only (or local on Server)
	[RPC]
	void setLocalPlayerIndex(byte idx)
	{
		localPlayerIndex = idx;
		CPlayer player = (CPlayer)allPlayers [idx];
		playerStatusScript.setLocalPlayer (ref player);

		//Last message received from server
		readyToReceive = true;
	}

	//Run on the clients only
	[RPC]
	void updateEntityCommon(byte entitytype, int entityidx, byte[] bytearray)
	{
		CEntity.EEntityType etype = (CEntity.EEntityType)entitytype;
		switch(etype)
		{
		case CEntity.EEntityType.PLAYER:
			byte idx = (byte)entityidx;
			if(allPlayers.ContainsKey(idx))
			{
				//Player already exists, so update
				CPlayer player = (CPlayer)allPlayers[idx];
				player.setCommon(bytearray);
				//allPlayers[idx] = player;//Shouldn't need this (it is by reference)
			} else {
				//Add new player
				CPlayer player = new CPlayer();
				player.setCommon(bytearray);
				allPlayers[idx] = player;
			}
			break;
		}
	}

	//Runs on Client only
	[RPC]
	public void updateEntityChanges(byte entitytype, int entityidx, Vector3 pos, Vector3 vel, Quaternion rot, double time)
	{
		CEntity.CEntityChanging change = new CEntity.CEntityChanging ();

		change.previousServerPosition =  pos;
		//change.previousServerVelocity = vel;
		change.previousServerRotation.x = rot.x;//TODO: Fix rot to be Vector2
		change.previousServerRotation.y = rot.y;
		change.validNetworkTime = (float)time;

		CEntity.EEntityType etype = (CEntity.EEntityType)entitytype;
		switch(etype)
		{
		case CEntity.EEntityType.PLAYER:
			byte idx = (byte)entityidx;
			if(allPlayers.ContainsKey(idx))
			{
				if(idx == localPlayerIndex)
				{
					//Do not apply changes
					//TODO: Improve to have reconsiliation
				} else {
					//Player already exists, so update
					CPlayer player = (CPlayer)allPlayers[idx];
					player.setChanging(change);
					//allPlayers[idx] = player;//Shouldn't need this (it is by reference)
				}
			} else {
				//Add new player
				CPlayer player = new CPlayer();
				player.setChanging(change);
				allPlayers[idx] = player;
			}
			break;
		}


	}
	//Run on the clients only
	[RPC]
	void updateEntityTransformers(byte entitytype, int entityidx, byte[] bytearray)
	{
		CEntity.EEntityType etype = (CEntity.EEntityType)entitytype;
		switch(etype)
		{
		case CEntity.EEntityType.PLAYER:
			byte idx = (byte)entityidx;
			if(allPlayers.ContainsKey(idx))
			{
				//Player already exists, so update
				CPlayer player = (CPlayer)allPlayers[idx];
				player.setTransformers(bytearray);
				//allPlayers[idx] = player;//Shouldn't need this (it is by reference)
			} else {
				//Add new player
				CPlayer player = new CPlayer();
				player.setTransformers(bytearray);
				allPlayers[idx] = player;
			}
			break;
		}

	}

	void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		Debug.Log("Player disconnected: " + player.name);

		if(isServer)
		{			
			//save player data and send remove player by RPC
			DeletePlayer (player.name);
		}
	}
	
	//Local call to setup player when enters room
	void SetupNewPlayer(string playername, byte playeridx) {
		SpawnMyPlayerEverywhere (playername,playeridx);
	}

	//Manual Spawning...
	//Called by Server
	void SpawnMyPlayerEverywhere(string playername, byte playeridx)
	{
		//Manually allocate PhotonViewID
		int id1 = PhotonNetwork.AllocateViewID();
		CPlayer player = (CPlayer)allPlayers [playeridx];
		player.setPhotonViewID (id1);

		//For all other players besides the Server, spawn on Server
		if(playeridx != localPlayerIndex)
		{
			SpawnOnServer (spawnlocation,
		               spawnrotation, id1, playername, playeridx);
		} else {
			//Else spawn like a local player on the server
			SpawnOnNetwork (spawnlocation,
			               spawnrotation, id1, playername, playeridx);
		}
		//Also spawn on other clients
		photonView.RPC("SpawnOnNetwork", PhotonTargets.Others, spawnlocation,
		               spawnrotation, id1, playername, playeridx);
	}
	//Run On Client/Server(for local player only)
	[RPC]
	void SpawnOnNetwork(Vector3 pos, Quaternion rot, int id1, string playername, byte playeridx)
	{
		GameObject newPlayer;
		CPlayer player = (CPlayer)allPlayers [playeridx];
		if(playername == localPlayerName)
		{
			newPlayer = (GameObject)Instantiate(playerPrefab, pos, rot);//mouse control
			playerStatusScript = newPlayer.GetComponentInChildren<PlayerStatus>();
			//Spawning myself, so make FollowCamera.Target point to this game object
			SmoothFollow ascript = mycamera.GetComponentInChildren<SmoothFollow>();
			MyCharacterMotor motorscript = newPlayer.GetComponent<MyCharacterMotor>();
			ascript.target = newPlayer.transform;
			player.setIsOwner(true);
			player.setMotor(motorscript);//set the motor for players
		} else {
			newPlayer = (GameObject)Instantiate(playerPrefabDummy, pos, rot);//no mouse control
		}
		//Set player Object
		player.setGameObject (newPlayer);

		//Set the PhotonView
		PhotonView[] nViews = newPlayer.GetComponentsInChildren<PhotonView>();
		//nViews [0].observed = newPlayer.transform;
		//nViews [0].onSerializeTransformOption = OnSerializeTransform.All;//Send position and rotatin
		//nViews [0].onSerializeRigidBodyOption = OnSerializeRigidBody.All;//send velocity and angular velocity
		nViews[0].viewID = id1;
	}

	//Called by Server for non local players
	void SpawnOnServer(Vector3 pos, Quaternion rot, int id1, string playername, byte playeridx)
	{
		GameObject newPlayer;
		CPlayer player = (CPlayer)allPlayers [playeridx];
		newPlayer = (GameObject)Instantiate(playerPrefabServer, pos, rot);//motor control
		MyCharacterMotor motorscript = newPlayer.GetComponent<MyCharacterMotor>();
		player.setMotor(motorscript);//set the motor for players

		//Set player Object
		player.setGameObject (newPlayer);
		
		//Set the PhotonView
		PhotonView[] nViews = newPlayer.GetComponentsInChildren<PhotonView>();
		nViews[0].viewID = id1;
	}

	//Load all known players data; setup dummy player for new players to use
	void loadPlayerData()
	{
		FileStream fs = new FileStream (playerfiledata, FileMode.OpenOrCreate);
		if (fs.Length > 0) {
			//There is data so load it
			BinaryFormatter br = new BinaryFormatter();
			playerIndex = (Dictionary<string,byte>) br.Deserialize(fs);
			allPlayers = (Dictionary<byte,object>) br.Deserialize(fs);
		} else {
			//No data, so make empty set
			allPlayers = new Dictionary<byte, object>();
			playerIndex = new Dictionary<string, byte> ();
		}
		fs.Close ();
		//find the largest playerIndex
		allPlayersMax = 0;
		CPlayer player = null;
		foreach(KeyValuePair<byte,object> item in allPlayers)
		{
			if(item.Key >= allPlayersMax)
			{
				allPlayersMax = item.Key;
				allPlayersMax++;
			}

			//Disable all players in the list initially
			player = (CPlayer)item.Value;
			player.disabled = true;
		}
	}

	void writePlayerData()
	{
		FileStream fs = new FileStream (playerfiledata, FileMode.Create);//Overwrite
		BinaryFormatter br = new BinaryFormatter();
		br.Serialize (fs, playerIndex);
		br.Serialize(fs,allPlayers);
		fs.Close ();
	}

	//Run on Server
	void DeletePlayer(string playername) {
		//Remove the player from allPlayers list 
		//and then remove on all clients by RPC
		byte idx = findPlayerIndex (playername);
		//Double check if player exists
		if(idx < byte.MaxValue)
		{
			CPlayer player = (CPlayer)allPlayers[idx];
			player.deleteObject ();//Remove game object
			player.disabled = true;//just disable on the server, but remove from client
			//allPlayers.Remove (idx);
			photonView.RPC ("DeletePlayerRPC", PhotonTargets.Others, idx);
		}
	}

	//Run on client
	[RPC]
	void DeletePlayerRPC(byte idx)
	{		
		CPlayer player = (CPlayer)allPlayers[idx];
		player.deleteObject ();
		allPlayers.Remove (idx);
	}

	//Called by Client to send to Server
	public void sendPlayerAction(CPlayer.CPlayerInput action)
	{		
		photonView.RPC("PlayerAction", PhotonTargets.MasterClient, localPlayerIndex, action.getBytes());
	}

	//Client receives this
	[RPC]
	public void playerActionAcknowledged(byte idx)
	{
		CPlayer player = (CPlayer)allPlayers[localPlayerIndex];
		player.setInputAcknowledge (idx);
	}

	//Remote call for button press by player, run on server
	[RPC]
	void PlayerAction(byte idx, byte[] bytearray, PhotonMessageInfo info)
	{
		CPlayer.CPlayerInput input = (CPlayer.CPlayerInput)Generic.ByteArrayToObject(bytearray);
		//sent from info.sender.ID;
		//time of vlidity info.timestamp;
		//PhotonNetwork.time is the current common network time
		CPlayer player = (CPlayer)allPlayers[idx];
		//Verify player exists
		if(player != null)
		{
			if(player.applyInput(input))
			{
				//Acknowledge input addressed
				photonView.RPC("playerActionAcknowledged", info.sender, input.id);
			}
		}
	}
	//==End All Player Functions==================================
	
	public void saveServer()
	{
		if(isServer)
		{
			writePlayerData ();
			saveAllChunks ();
			Debug.Log("Server Data Saved");
		}
	}
	//==Chunk Data Functions=====================================

	Vector3 getChunkPosition(long _x, long _y, long _z)
	{
		return new Vector3 (currentChunkSize * _x, currentChunkSize * _y, currentChunkSize * _z);
	}

	void loadChunk(int _x, int _y, int _z)
	{
		if (!voxelopen)
		{
			voxel.open (voxelfileid, voxelfiledata);
			voxel.load();
			voxelopen = true;
			currentChunkSize = voxel.getChunkSize();
		}
		ColoredCubesVolumeData data;
		data = voxel.getChunk (_x, _y, _z);

		GameObject obj = ColoredCubesVolume.CreateGameObject (data, true, true);

		//Transform the Chunk
		obj.transform.position = getChunkPosition(_x, _y, _z);

		//Attach a script to compress the chunk and uncompress on send/receive serialize
		//ChunkScript ascript = obj.AddComponent<ChunkScript>();
		//ascript.setData (this);
		
		//Save chunk
		chunkData achunk = new chunkData (_x, _y, _y, obj);
		allLoadedChunks.Add (achunk);
		
	}

	//Sends updated Chunk to all Near Players
	void sendChunkUpdate(long _x, long _y, long _z)
	{
		//Cycle through all players and see who is near the chunk
		CPlayer player = null;
		foreach(KeyValuePair<byte,object> item in allPlayers)
		{
			if(item.Key != localPlayerIndex)//Skip local Player
			{
				sendAChunkNearPlayer(player,_x,_y,_z);
			}
		}
	}

	//Send all Chunks to Player that are near by
	void sendAllChunkNearPlayer(CPlayer player)
	{
		
		sendChunkData data=new sendChunkData();
		data.player = player.getPhotonPlayer();
		Vector3 ploc = player.getLocation();
		//add 0.5 for rounding up or down
		long xstart = (long)(ploc.x - PLAYER_NEAR_CHUNK - 0.5);
		long xend = (long)(ploc.x + PLAYER_NEAR_CHUNK + 0.5);
		long ystart = (long)(ploc.y - PLAYER_NEAR_CHUNK - 0.5);
		long yend = (long)(ploc.y + PLAYER_NEAR_CHUNK + 0.5);
		long zstart = (long)(ploc.z - PLAYER_NEAR_CHUNK - 0.5);
		long zend = (long)(ploc.z + PLAYER_NEAR_CHUNK + 0.5);
		//Near by is computed by xyz+/-NEAR DISTANCE
		for(long xx = xstart; xx < xend; ++xx)
		{
			data.x = xx;
			for(long yy = ystart; yy < yend; ++yy)
			{
				data.y = yy;
				for(long zz = zstart; zz < zend; ++zz)
				{
					//Add to chunk Send
					data.z = zz;
					chunksToSend.Add(data);
				}
	
			}

		}
	}

	//Check if chunk is near player and send if is
	void sendAChunkNearPlayer(CPlayer player,long _x, long _y, long _z)
	{
		//Calculate distance
		Vector3 ploc = player.getLocation();
		Vector3 cloc = getChunkPosition(_x,_y,_z);
		Vector3 trans = (ploc - cloc);
		float mag = trans.magnitude;
		if(mag < PLAYER_NEAR_CHUNK)
		{
			sendChunkData data;
			//Add to chunk Send
			data = new sendChunkData(player.getPhotonPlayer(),_x,_y,_z);
			chunksToSend.Add(data);
		}
	}

	void saveAllChunks()
	{
		//Cycle through all chunks and save them
		chunkData achunk;
		ColoredCubesVolume data;
		for(int i = 0; i<allLoadedChunks.Count; i++)
		{
			achunk = (chunkData)allLoadedChunks[i];
			data = achunk.voxel.GetComponent<ColoredCubesVolume>();
			if(data != null && data.data != null)//just a safeguard
			{
				voxel.saveChunk(achunk.x,achunk.y,achunk.z,data.data);
			}
		}
		voxel.saveid ();//save the id too
	}
	
	
	
	void InitializeWorld()
	{
		//Set basic color
		QuantizedColor basicVoxColor = new QuantizedColor ();
		basicVoxColor.red = 127;
		basicVoxColor.green = 0;
		basicVoxColor.blue = 0;
		basicVoxColor.alpha = 255;
		voxel.setBasicColor (basicVoxColor);
		loadChunk (0, 0, 0);
		
		// Perform the raycasting. If there's a hit the position will be stored in these ints.
		//PickVoxelResult pickResult;
		//bool hit = Picking.PickFirstSolidVoxel(coloredCubesVolume, ray, 1000.0f, out pickResult);
		// If we hit a solid voxel then create an explosion at this point.
		//if(hit)
		//{					
		//	int range = 5;
		//	DestroyVoxels(pickResult.volumeSpacePos.x, pickResult.volumeSpacePos.y, pickResult.volumeSpacePos.z, range);
		//}
	}
	/*==========================================
	//For voxels, determine if surface voxel
	public bool IsSurfaceVoxel(int x, int y, int z)
	{
		QuantizedColor quantizedColor;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z);
		if(quantizedColor.alpha < 127) return false;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x + 1, y, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x - 1, y, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y + 1, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y - 1, z);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z + 1);
		if(quantizedColor.alpha < 127) return true;
		
		quantizedColor = coloredCubesVolume.data.GetVoxel(x, y, z - 1);
		if(quantizedColor.alpha < 127) return true;
		
		return false;
	}
	//==========================================*/

	void SendNetworkChunk()
	{
		if (chunksToSend.Count > 0) {
			sendChunkData chunktosend = (sendChunkData)chunksToSend [0];//grab first chunk (fifo)
			//compress data and seek
			long chunkbytesize = CONST_QUANTIZE_COLOR * currentChunkSize * currentChunkSize * currentChunkSize;//cube uncompressed
			byte[] decompressed = new byte[chunkbytesize];
	
			long byteoffset = 0;
			QuantizedColor qcolor = new QuantizedColor ();
			ColoredCubesVolumeData data = voxel.getChunk(chunktosend.x,chunktosend.y,chunktosend.z);
			for (int x=0; x<currentChunkSize; x++) {
						for (int y=0; y<currentChunkSize; y++) {
								for (int z=0; z<currentChunkSize; z++) {
										byteoffset = x * y * z * CONST_QUANTIZE_COLOR;
										qcolor = data.GetVoxel (x, y, z);
										decompressed [byteoffset] = qcolor.red;
										decompressed [byteoffset + 1] = qcolor.green;
										decompressed [byteoffset + 2] = qcolor.blue;
										decompressed [byteoffset + 3] = qcolor.alpha;
								}
						}
			}
			
			byte[] compressed = CLZF2.Compress (decompressed);
			int msgcompression = (int)(100.0f*((float)chunkbytesize-(float)compressed.Length)/(float)chunkbytesize);
			Debug.Log ("Sending Chunk at " + msgcompression + "% compressed");
			photonView.RPC("ReceiveNetworkChunk", chunktosend.player, chunktosend.x,chunktosend.y,chunktosend.z,currentChunkSize, compressed);
			chunksToSend.RemoveAt (0);
		}
	}
	[RPC]
	void ReceiveNetworkChunk(int _x, int _y, int _z, int sizeofChunk, byte[] compressed)
	{
		byte[] decompressed = CLZF2.Decompress(compressed);
		long chunkbytesize = CONST_QUANTIZE_COLOR*sizeofChunk*sizeofChunk*sizeofChunk;//cube uncompressed
		if(decompressed.Length == chunkbytesize)
		{
			ColoredCubesVolumeData data = VolumeData.CreateEmptyVolumeData<ColoredCubesVolumeData>(new Region(0, 0, 0, sizeofChunk-1, sizeofChunk-1, sizeofChunk-1));
			long byteoffset = 0;
			QuantizedColor qcolor = new QuantizedColor();
			for(int x=0; x<sizeofChunk; x++)
			{
				for(int y=0; y<sizeofChunk; y++)
				{
					for(int z=0; z<sizeofChunk; z++)
					{
						byteoffset = x*y*z*CONST_QUANTIZE_COLOR;
						qcolor.red = decompressed[byteoffset];
						qcolor.green = decompressed[byteoffset+1];
						qcolor.blue = decompressed[byteoffset+2];
						qcolor.alpha = decompressed[byteoffset+3];
						data.SetVoxel(x,y,z,qcolor);
					}
				}
			}

			
			GameObject obj = ColoredCubesVolume.CreateGameObject (data, true, true);
			//Transform the Chunk
			obj.transform.position = getChunkPosition(_x,  _y,  _z);
			//And add PhotonView to it
			//int id1 = PhotonNetwork.AllocateViewID();
			//Set the PhotonView
			//PhotonView nView = obj.AddComponent<PhotonView>();
			//nView.viewID = id1;
			//Attach a script to compress the chunk and uncompress on send/receive serialize
			//ChunkScript ascript = obj.AddComponent<ChunkScript>();
			//ascript.setData (data,currentChunkSize);
		}
		else
		{
			Debug.Log ("Received Chunk that didn't decompress to correct size (" + decompressed.Length + " of " + chunkbytesize + " bytes)");
		}
	}
	//==END Chunk Data Transmition=====================================

	
	//=====OTHER DEBUG CallBacks==================
	
	// MAIN CALLBACKS
	//PLAYER EVENTS
	void OnPhotonPlayerConnected(PhotonPlayer player)
	{
		Debug.Log("Player connected: " + player.name);
	}

	void OnConnectedToPhoton()
	{
		Debug.Log("This client has connected to a server");
	}
	
	void OnDisconnectedFromPhoton()
	{
		Debug.Log("This client has disconnected from the server");
	}
	
	void OnFailedToConnectToPhoton(ExitGames.Client.Photon.StatusCode status)
	{
		Debug.Log("Failed to connect to Photon: " + status);
	}

	
	//ROOM EVENTS
	
	void OnCreatedRoom()
	{
		Debug.Log("We have created a room.");
		//When creating a room, both OnCreatedRoom AND OnJoinedRoom will be called.
	}
	
	void OnJoinedRoom()
	{
		Debug.Log("We have joined a room.");
	}
	
	void OnLeftRoom()
	{
		Debug.Log("This client has left a game room.");
	}
	
	void OnPhotonCreateRoomFailed()
	{
		Debug.Log("A CreateRoom call failed, most likely the room name is already in use.");
	}
	
	void OnPhotonJoinRoomFailed()
	{
		Debug.Log("A JoinRoom call failed, most likely the room name does not exist or is full.");
	}
	
	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("A JoinRandom room call failed, most likely there are no rooms available.");
	}
	
	// LOBBY EVENTS
	
	void OnJoinedLobby()
	{
		Debug.Log("We joined the lobby.");
	}
	
	void OnLeftLobby()
	{
		Debug.Log("We left the lobby.");
	}
	
	// ROOMLIST
	
	void OnReceivedRoomList()
	{
		Debug.Log("We received a new room list, total rooms: " + PhotonNetwork.GetRoomList().Length);
	}
	
	void OnReceivedRoomListUpdate()
	{
		Debug.Log("We received a room list update, total rooms now: " + PhotonNetwork.GetRoomList().Length);
	}
	
	
	void OnMasterClientSwitched(PhotonPlayer newMaster)
	{
		Debug.Log("The old masterclient left, we have a new masterclient: " + newMaster);
	}
	
	//This is called only when the current gameobject has been Instantiated via PhotonNetwork.Instantiate
	void OnPhotonInstantiate(PhotonMessageInfo info)
	{
		Debug.Log("New object instantiated by " + info.sender);
	}
	//=====END OTHER DEBUG CallBacks==================

}
