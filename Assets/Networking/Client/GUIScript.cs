﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(PhotonView))]
public class GUIScript : Photon.MonoBehaviour {
	private static bool roomSetup = false;
	private static bool isDedicated = false;
	public static bool networkGameSetup = false;
	public static string defaultUsername = "";
	public static string inputUsername = defaultUsername;
	public static bool gameStarted = false;
	public static bool getRoomSetup() {
				return roomSetup;
		}
	public static bool getIsDedicated() {
		return isDedicated;
		}
	private static bool isServer = false;
	public static bool getIsServer() {
		return isServer;
	}
	void Awake()
	{
		PhotonNetwork.ConnectUsingSettings("1.0");
		roomSetup = false;
		DontDestroyOnLoad (this);//Keep this information between scenes
	}
	

	//public Font font;
	
	//public GUIStyle guiStyle;
	
	void Start(){
		//guiStyle.font = font;

		//Join a room
		//PhotonNetwork.JoinRoom("Arena");
		
		//Tries to join any random game:
		//PhotonNetwork.JoinRandomRoom();
		//Fails if there are no matching games and calls OnPhotonRandomJoinFailed
		//PhotonNetwork.GetRoomList ();
		//Create this room.
		//PhotonNetwork.CreateRoom("MyMatch");
		// Fails if it already exists and calls OnPhotonCreateGameFailed
	}

	void SetupPUNRoom()
	{
		RoomOptions roomOptions = new RoomOptions () { isVisible = false, maxPlayers = 4 };
		bool sent = PhotonNetwork.JoinOrCreateRoom ("Room1", roomOptions, TypedLobby.Default);
		Debug.Log ("Created room1 " + sent.ToString());
		roomSetup = true;
	}

	void OnGUI(){
		
		//Check connection state..
		if (!PhotonNetwork.connected && !PhotonNetwork.connecting)
		{
			//We are currently disconnected
			GUI.Label(new Rect(40,190,80,20),"Connection status: Disconnected");
			
			//GUILayout.BeginVertical();
			//if (GUILayout.Button("Connect"))
			//{
				//Connect using the PUN wizard settings (Self-hosted server or Photon cloud)
			//	PhotonNetwork.ConnectUsingSettings("1.0");
			//}
			//GUILayout.EndVertical();
		}
		else
		{
			//We're connected or connecting!
			if (PhotonNetwork.connected)
			{
				GUI.Label(new Rect(40,190,80,20),"Connection status: Connected");
				GUI.Label(new Rect(40,210,200,20),"Ping to server: " + PhotonNetwork.GetPing());
				if(!gameStarted)
				{
					GUI.Label(new Rect(40,250,60,20),"Username:");
					inputUsername = GUI.TextField(new Rect(100,250,80,20),inputUsername);
					PhotonNetwork.playerName = inputUsername;
				}

				//TODO: Display list of rooms
			//	if (GUILayout.Button("Disconnect"))
			//	{
					//Completely disconnect from the Photon server
			//		PhotonNetwork.Disconnect();
			//	}
			}
			else
			{
				//Connecting...
				GUI.Label(new Rect(40,190,120,20),"Connection status: " + PhotonNetwork.connectionState);
			}
		}
	}
	
	
	
	// Note:
	// NONE of the functions below is of any use in this demo, the code below is for demonstration/reference only.
	// First ensure you understand the code in the OnGUI() function above.
	
	//
	// You can add any of the methods below to any of your scripts:
	//      they will be called when the corresponding event happens
	//
	
	// MAIN CALLBACKS
	void OnConnectedToPhoton()
	{
		Debug.Log("This client has connected to a server");
	}
	
	void OnDisconnectedFromPhoton()
	{
		Debug.Log("This client has disconnected from the server");
	}
	
	void OnFailedToConnectToPhoton(ExitGames.Client.Photon.StatusCode status)
	{
		Debug.Log("Failed to connect to Photon: " + status);
	}
	
	
	
	//ROOM EVENTS
	
	void OnCreatedRoom()
	{
		Debug.Log("We have created a room.");
		//When creating a room, both OnCreatedRoom AND OnJoinedRoom will be called.
		isServer = true;
		PhotonNetwork.isMessageQueueRunning = false;//Disable updates at this point
		networkGameSetup = true;
	}
	
	void OnJoinedRoom()
	{
		Debug.Log("We have joined a room.");
		isServer = false;
		PhotonNetwork.isMessageQueueRunning = false;//Disable updates at this point
		networkGameSetup = true;
	}
	
	void OnLeftRoom()
	{
		Debug.Log("This client has left a game room.");
	}
	
	void OnPhotonCreateRoomFailed()
	{
		Debug.Log("A CreateRoom call failed, most likely the room name is already in use.");
	}
	
	void OnPhotonJoinRoomFailed()
	{
		Debug.Log("A JoinRoom call failed, most likely the room name does not exist or is full.");
	}
	
	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("A JoinRandom room call failed, most likely there are no rooms available.");
	}
	
	// LOBBY EVENTS
	
	void OnJoinedLobby()
	{
		Debug.Log("We joined the lobby.");
		if(!roomSetup)
		{
			SetupPUNRoom();
		}
	}
	
	//void OnLeftLobby()
	//{
	//	Debug.Log("We left the lobby.");
	//}
	
	// ROOMLIST
	
	//void OnReceivedRoomList()
	//{
	//	Debug.Log("We received a new room list, total rooms: " + PhotonNetwork.GetRoomList().Length);
	//}
	
	//void OnReceivedRoomListUpdate()
	//{
	//	Debug.Log("We received a room list update, total rooms now: " + PhotonNetwork.GetRoomList().Length);
	//}
	
	
	
	
	
	//PLAYER EVENTS
	//void OnPhotonPlayerConnected(PhotonPlayer player)
	//{
	//	Debug.Log("Player connected: " + player);
	//}
	
	//void OnPhotonPlayerDisconnected(PhotonPlayer player)
	//{
	//	Debug.Log("Player disconnected: " + player);
	//	
	//}
	void OnMasterClientSwitched(PhotonPlayer newMaster)
	{
		Debug.Log("The old masterclient left, we have a new masterclient: " + newMaster);
	}
	
	//This is called only when the current gameobject has been Instantiated via PhotonNetwork.Instantiate
	void OnPhotonInstantiate(PhotonMessageInfo info)
	{
		Debug.Log("New object instantiated by " + info.sender);
	}
	
	//This is used only when observing this script via a PhotonView, more on this later.
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		//Custom code here (your code!)
	}

}
