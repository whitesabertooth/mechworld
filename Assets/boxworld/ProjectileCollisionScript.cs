using UnityEngine;
using System.Collections;

public class ProjectileCollisionScript : MonoBehaviour {
	public int doDamage = 1;
	private bool damageDone = false;
	void OnCollisionEnter(Collision col)
	{
		if (!damageDone) {//Needed because sometimes there are multiple collisions at the same time, so just take one
				damageDone = true;
				Debug.Log ("Hit " + col.gameObject.name + ", doing " + doDamage + " damage");
				col.gameObject.SendMessage ("DamageTaken", doDamage, SendMessageOptions.DontRequireReceiver);
				Destroy (gameObject);
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if (!damageDone) {//Needed because sometimes there are multiple collisions at the same time, so just take one
				damageDone = true;
				Debug.Log ("Trigger Hit " + col.gameObject.name + ", doing " + doDamage + " damage");
				col.gameObject.SendMessage ("DamageTaken", doDamage, SendMessageOptions.DontRequireReceiver);
				Destroy (gameObject);
		}
	}
}
