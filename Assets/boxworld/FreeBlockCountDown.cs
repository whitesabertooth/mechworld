using UnityEngine;
using System.Collections;

public class FreeBlockCountDown : MonoBehaviour {
	public float countDownSec = 10.0f;
	private float countDownSecCurrent;
	private bool setCountDown = false;
	void Update()
	{
		if(!setCountDown)
		{
			countDownSecCurrent = countDownSec;
			setCountDown = true;
		} else {
			countDownSecCurrent -= Time.deltaTime;
		}
		
		if(countDownSecCurrent < 0)
		{
			Debug.Log ("Destroying Free Block " + countDownSec + " has been reached");
			Destroy(gameObject);
		}
		
	}
}

