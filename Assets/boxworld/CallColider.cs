using UnityEngine;
using System.Collections;

public class CallColider : MonoBehaviour {
	private OnBreakGoFree onbgf;
	void Awake() {
		onbgf = GetComponent("OnBreakGoFree") as OnBreakGoFree;
	}
	void OnCollisionEnter(Collision col) {
		
		//if(gameObject.layer == 11) //"FreeBlock"
		//{
			if(col.transform.parent != null)
			{
				if(onbgf.attachedObject == null)
				{
					Debug.Log ("Forward Collision");
					col.transform.parent.gameObject.SendMessage("boxCollision",gameObject,SendMessageOptions.DontRequireReceiver);
				}
			      else
				{
					Debug.Log ("Forward Collision");
					if(col.transform.parent != onbgf.attachedObject)
					{
						col.transform.parent.gameObject.SendMessage("boxCollision",onbgf.attachedObject,SendMessageOptions.DontRequireReceiver);
					}
				
				}
			}
		//}
	}
}
