using UnityEngine;
using System.Collections;

public class OnBreakGoFree : MonoBehaviour {
	public float shootAwaySpeed = 10;
	public GameObject attachedObject = null;
	void FreeBlock(/*Transform coreObject = null*/) {//Input is core object to explode from, or no input means use the root
		//Change block to be free
		if (gameObject.layer != 11) { //Isn't already a FreeBlock
						Transform coreObject = null;
						//if(coreObject == null)
						{
								if (attachedObject == null) {
										coreObject = transform.root;
								} else {
										coreObject = attachedObject.transform;
								}
						}
	
						//[TODO]:Destroy all joints and trace all joints below to become free also
		
		
						damageTaken = 0;

						gameObject.layer = 11; //"FreeBlock"
						transform.parent = null;//Disconnect from parent - MIGHT BE A PROBLEM
						rigidbody.isKinematic = false;
		
						//Core object to shoot away from
						//Transform coreObject = transform.root;
						Vector3 dir = transform.position - coreObject.transform.position;
						rigidbody.AddForce (dir, ForceMode.VelocityChange);
		
						//Add countdown to destruction
						gameObject.AddComponent ("FreeBlockCountDown");
						Debug.Log ("FreeBlockCountDown added to " + gameObject.name);
						//gameObject.AddComponent("CallCollider");
		
						attachedObject = null;
						//Debug.Log ("FreeBlock " + gameObject.name);
		
						//Remove collision forwarding
						//BoxCollision bc = gameObject.GetComponent<BoxCollision>();
						//if(fbcd != null)
						//{
						//	Destroy(bc);
						//}
				} else {
			Debug.Log ("Skipped FreeBlock for " + gameObject.name);
				}
    }
	
	
	void AttachBlock(GameObject coreObject) {
		//Change block to be attached to coreObject
		attachedObject = coreObject;//save who the attached core is
		gameObject.layer = coreObject.gameObject.layer;
		//gameObject.layer = 14;//"AttachedBlock
		transform.parent = coreObject.gameObject.transform;
		rigidbody.isKinematic = true;
		//rigidbody.useGravity = false;
		//collider.isTrigger = true;
		
		//[TODO]:set object to stop moving

		
		//Remove countdown to destruction
		FreeBlockCountDown fbcd = gameObject.GetComponent<FreeBlockCountDown>();
		if (fbcd != null) {
				Debug.Log ("AttachBlock " + gameObject.name);
				Destroy (fbcd);
		}
		
    }
	
	
	void AttachItem_gameObject(GameObject coreObject) {
		if(attachedObject != null)
		{
			attachedObject.transform.parent = coreObject.gameObject.transform;
			attachedObject.BroadcastMessage("removeTrigger");
		} else {
			transform.parent = coreObject.gameObject.transform;
			BroadcastMessage("removeTrigger");
		}
		//[TODO]:set item from stop moving
		
    }
	void AttachItem_position(Vector3 pos) {
		if(attachedObject != null)
		{
			attachedObject.transform.localPosition = pos;
		} else {
			transform.localPosition = pos;
		}
		
		
    }
	void AttachItem_rotation(Quaternion rot) {
		Debug.Log("Attach Rotation: " + rot.ToString());
		if(attachedObject != null)
		{
			attachedObject.transform.localRotation = rot;
		} else {
			transform.localRotation = rot;
		}
		
		
    }
	
	public int lifePoints = 10;
	private int damageTaken = 0;
	
	void DamageTaken(int damage)
	{
		damageTaken += damage;
		Debug.Log("HP for " + gameObject.name + " = " + damageTaken + "/" + lifePoints);
		if(damageTaken >= lifePoints)
		{
			if(gameObject.layer == 11)//"FreeBlock"
			{
				//Hit FreeBlock
				Destroy(gameObject);
			} else if(gameObject.layer == 12 || gameObject.layer == 14 || gameObject.layer == 9)//"PickupItem" or "AttachedBlock" or "Usable"
			{
				if(attachedObject != null)
				{
					//Send to all sub-objects of the head object (which is attachedObject)
					//Save off children 1st (and head), because they will be removed
					int arrsize = attachedObject.transform.childCount;
					GameObject [] gamearr = new GameObject[arrsize+1];
					for(int i=0; i<arrsize; i++)
					{
						gamearr[i] = attachedObject.transform.GetChild (i).gameObject;
					}
					gamearr[arrsize] = attachedObject;//Add head of group
					for(int i=0; i<arrsize+1; i++)
					{
						gamearr[i].gameObject.SendMessage("removeTrigger",SendMessageOptions.DontRequireReceiver);
						gamearr[i].gameObject.SendMessage("FreeBlock",SendMessageOptions.DontRequireReceiver);
					}
				} else {
					//Save off children 1st, because they will be removed
					int arrsize = transform.childCount;
					GameObject [] gamearr = new GameObject[arrsize];
					for(int i=0; i<arrsize; i++)
					{
						gamearr[i] = transform.GetChild (i).gameObject;
					}
					for(int i=0; i<arrsize; i++)
					{
						gamearr[i].gameObject.SendMessage("removeTrigger",SendMessageOptions.DontRequireReceiver);
						gamearr[i].gameObject.SendMessage("FreeBlock",SendMessageOptions.DontRequireReceiver);
					}
					gameObject.SendMessage("removeTrigger",SendMessageOptions.DontRequireReceiver);
					gameObject.SendMessage("FreeBlock",SendMessageOptions.DontRequireReceiver);
					transform.parent = null;//Disconnect from parent
				}
			} else {
				//Hit player attached free block
				damageTaken = 0;
				FreeBlock();
			}
		}
	}
	
	//Forward to attachedObject
	void ActionScript(GameObject obj)
	{
		//Forward to core
		if(attachedObject != null)
		{
			attachedObject.SendMessage("ActionScript",obj,SendMessageOptions.DontRequireReceiver);
		}
	}
	
	//Forward to attachedObject
	void PlayerActionScript(GameObject obj)
	{
		//Forward to core
		if(attachedObject != null)
		{
			Debug.Log("Fire1 - Forwarded by " + gameObject.name + " to " + attachedObject.name);
			attachedObject.SendMessage("PlayerActionScript",obj,SendMessageOptions.DontRequireReceiver);
		}
	}
	
	
	
	void OnCollisionEnter(Collision col) {
		CollisionProcess(col.gameObject);
		
		
	}
	
	void CollisionProcess(GameObject obj)
	{
		if(attachedObject != null)
		{
			if(attachedObject.transform.parent == null || (attachedObject.transform.parent != null && attachedObject.transform.parent.tag != "Player"))	
			{
					obj.SendMessageUpwards("boxCollision",attachedObject,SendMessageOptions.DontRequireReceiver);
			}
		} else {
			if(transform.parent == null || (transform.parent != null && transform.parent.tag != "Player"))	
			{
					obj.SendMessageUpwards("boxCollision",gameObject,SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	
	void OnTriggerEnter(Collider col) {
		CollisionProcess(col.gameObject);
		
	}
	
	void removeTrigger() {
			collider.isTrigger = false;
	}
	
	
	void setTrigger() {
			collider.isTrigger = true;
	}
	
}
