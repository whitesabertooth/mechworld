using UnityEngine;
using System.Collections;

public class CollidePlayer : MonoBehaviour {
    public float pushPower = 150.0F;
    void OnControllerColliderHit(ControllerColliderHit hit) {
		
		if(hit.gameObject.layer != 10)//"Player"
		{
	        Rigidbody body = hit.collider.attachedRigidbody;
	        if (body == null || body.isKinematic)
	            return;
	        
	        if (hit.moveDirection.y < -0.3F)
	            return;
	        
	        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
	        //body.AddForce(pushDir * pushPower);
			body.AddForce(pushDir * pushPower,ForceMode.VelocityChange);
		}
    }
}
