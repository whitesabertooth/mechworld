using UnityEngine;
using System.Collections;

/// <summary>
/// Item/Box Structs
/// </summary>
using System.IO;
using System.Runtime.Serialization;
using System;



/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
//TODO: Fix to have multiple componentMenus [AddComponentMenu("Camera-Control/Mouse Look")]
//TODO: Fix this line [RequireComponent ("MyCharacterMotor")]
[AddComponentMenu ("Character/FPS Input Controller")]
public class PlayerStatus : Photon.MonoBehaviour {
	
	//MouseLook Tweakable
	public CPlayer.RotationAxes axis = CPlayer.RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;	
	public float minimumX = -360F;
	public float maximumX = 360F;	
	public float minimumY = -60F;
	public float maximumY = 60F;
	public float timewait = 0.2F;

	private NetworkCommunication networkCommunicationScript;
	private CPlayer localPlayer;
	private static readonly int NUM_ACTIONS = 2;
	private float []lasttime;

	void Awake()
	{
		localPlayer = null;
		lasttime = new float[NUM_ACTIONS];
		for(int i=0; i<NUM_ACTIONS; i++)
		{
			lasttime[i] = 0.0f;
		}
	}

	void Start()
	{
		GameObject obj = GameObject.FindGameObjectWithTag ("GlobalData");
		networkCommunicationScript = obj.GetComponent<NetworkCommunication> ();
	}


    void OnControllerColliderHit(ControllerColliderHit hit) {
		if(localPlayer != null)
		{
			localPlayer.boxCollision(hit.gameObject);
		}
    }

	public void setLocalPlayer(ref CPlayer player)
	{
		localPlayer = player;

		//MouseLook Adjustments
		localPlayer.axes = axis;
		localPlayer.sensitivityX = sensitivityX;
		localPlayer.sensitivityY = sensitivityY;	
		localPlayer.minimumX = minimumX;
		localPlayer.maximumX = maximumX;	
		localPlayer.minimumY = minimumY;
		localPlayer.maximumY = maximumY;
	}

	void Update()
	{
		if(localPlayer != null)
		{
			localPlayer.checkPlayerKeys ();
			localPlayer.checkPlayerRotation ();

			if(Input.GetButton("Fire1"))
			{
				if(lasttime[0] + timewait < (float)Time.fixedTime)
				{
					Debug.Log("Fire1");
					localPlayer.useAction(0);
					lasttime[0] = (float)Time.fixedTime;
				}
			}
			if(Input.GetButton("Fire2"))
			{
				if(lasttime[1] + timewait < (float)Time.fixedTime)
				{
					Debug.Log("Fire2");
					localPlayer.useAction(1);
					lasttime[1] = (float)Time.fixedTime;
				}
			}
			if(Input.GetKeyDown (KeyCode.E))
			{
				Debug.Log("E");
				localPlayer.useKey();
			}
			if(Input.GetKeyDown (KeyCode.Escape))
			{
				Debug.Log("Escape");
				networkCommunicationScript.saveServer();
				//Application.LoadLevel(0);//Go back to beginning  -- More like pause the local game
			}
		}
	}

	void FixedUpdate()
	{
		if(localPlayer != null)
		{
			//Send all Inputs
			//Loop until no more inputs to send
			CPlayer.CPlayerInput input = localPlayer.getNextInputNotSent();
			while(input != null)
			{
				networkCommunicationScript.sendPlayerAction(input);
				//Get next input
				input = localPlayer.getNextInputNotSent();
			}
		}
	}

	/// <summary>
	/// Keyboard Actions
	/// </summary>
	void OnGUI() {
		//Only draw HUD and check inputs if localPlayer set
		if(localPlayer != null)
		{
			//Draw Player HUD
			//Player Name and life
			int playerpercent = (int)(100.0f*((float)localPlayer.getLifePoints() - (float)localPlayer.getDamageTaken())/(float)localPlayer.getLifePoints());
			GUI.Label(new Rect(150,350,100,20),localPlayer.getPlayerName() + " " + playerpercent + "%");
			GUI.Label(new Rect(150,250,100,20),"Inputs Sent: " + localPlayer.inputSize());


		}		
    }



}
