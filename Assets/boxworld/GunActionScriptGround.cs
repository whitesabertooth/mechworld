using UnityEngine;
using System.Collections;

public class GunActionScriptGround : MonoBehaviour {
	public GameObject newBox;
	public Vector3 createBullet;
	public float speedBullet;
	private ProjectileCollisionScript pcs;
	void ActionScript(GameObject obj)
	{
		Debug.Log("Gun Action Script");
        GameObject clone;
		//Debug.Log ("Bullet Position = " + bullet.ToString());
        clone = Instantiate(newBox, transform.TransformPoint(createBullet), transform.rotation) as GameObject;
		clone.rigidbody.AddForce(transform.TransformDirection(Vector3.right * speedBullet),ForceMode.VelocityChange);
		clone.layer = 13; //"Projectile"
		pcs = clone.AddComponent("ProjectileCollisionScript") as ProjectileCollisionScript;
		pcs.doDamage = 2;
    }
}
